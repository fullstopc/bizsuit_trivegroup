<?php

use Philo\Blade\Blade;
use Tightenco\Collect;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class HelperController
{    
    protected $_config;
    function __construct()
    {
        $this->_config = include '../config.php';
    }

    function ImportData($query, $body)
    {
        $data = $body;
        $directoryName = '../resources/data/';
        if(!is_dir($directoryName)){
            mkdir($directoryName, 0755, true);
        }

        foreach($data as $key => $item)
        {
            $filename = "$key.json";
            $file = $directoryName.$filename;
            if(!is_file($file)){
                file_put_contents($file, '');
            }
            file_put_contents($file, json_encode($item));
        }
    }

    function ImportImages($query, $body)
    {
        $data = $body;
        $directories = [
            './img/content/', './img/product/', './img/uploads/'
        ];

        // create all directory
        foreach($directories as $directory)
        {
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }
        }

        foreach($data['products'] as $image)
        {
            $id = $image['id'];
            $name = $image['name'];
            $url = $image['url'];

            $directory = "./img/product/$id/";
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }

            copy($url, $directory.$name);
        }

        foreach($data['contents'] as $image)
        {
            $id = $image['id'];
            $name = $image['name'];
            $url = $image['url'];

            $directory = "./img/content/$id/";
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }

            copy($url, $directory.$name);
        }

        foreach($data['uploads'] as $image)
        {
            $name = $image['name'];

            $directory = "./img/uploads/";
            if(!is_dir($directory)){
                mkdir($directory, 0755, true);
            }
            copy($url, $directory.$name);
        }
    }

    function GetData()
    {
        $data = include_once '../public/data/data.php';
        $data['categories'] = collect($data['categories'])->map(function($item, $key){
            $item['desc'] = null;
            $item['products'] = null;
            return $item;
        });
        $data['products'] = collect($data['products'])->map(function($item, $key){
            $item['desc'] = null;
            $item['categories'] = collect($item['categories'])->map(function($category){
                return $category['id'];
            });
            return $item;
        });
        $data['categories'] = null;
        //$data['products'] = null;
        $data['blogs'] = null;
        echo json_encode($data);
    }

    function GetPages()
    {
        $data = file_get_contents("../resources/data/pages.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function GetCategorytree(){
        $categoryAll = file_get_contents("../resources/data/categories.json");
        $categoryAll = json_decode($categoryAll, true);
        $categoryAll = collect($categoryAll);
        $categoryRoot = array();
        foreach($categoryAll as $category){
            if ($category["parent_id"] == null){
                array_push($categoryRoot, $category);
            }
        }
        foreach($categoryRoot as &$root){
            foreach($categoryAll as $category){
                if($category["parent_id"] == $root["id"]){
                    $root["children"][] = $category;
                }
            }
        }
        return $categoryRoot;
    }
    function GetCategoriesChildren(){
        $categoryAll = file_get_contents("../resources/data/categories.json");
        $categoryAll = json_decode($categoryAll, true);
        $categoryAll = collect($categoryAll);
        $realSubCategory = array();
        foreach($categoryAll as $category){
            if ($category["parent_id"] == !null){
                array_push($realSubCategory, $category);
            }
        }
        return $realSubCategory;
    }

    function GetCategories()
    {
        $pivot = $this->GetProductCategories();
        $data = file_get_contents("../resources/data/categories.json");
        $data = json_decode($data, true);
        $data = collect($data);

        $data->transform(function($item) use ($pivot) {
            $item["productIds"] = $pivot->where('category_id', $item['id'])->pluck('product_id');
            return $item;
        });
        return $data;
    }

    function GetProducts()
    {
        $pivot = $this->GetProductCategories();
        $data = file_get_contents("../resources/data/products.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function GetProductCategories()
    {
        $data = file_get_contents("../resources/data/products_categories.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function GetBlogs()
    {
        $data = file_get_contents("../resources/data/blogs.json");
        $data = json_decode($data, true);
        $data = collect($data);
        return $data;
    }

    function PostEnquiry($query)
    {
        $response = [
            'status' => 'fail',
            'message' => '',
        ];
        $mail = $this->GetMailConfig();
        
        try {
            
            $mail->Subject = "Enquiry - [{$this->_config['title_name']}]";
            $mail->Body    = "
                <table>
                <tr>
                    <td>Salutation</td>
                    <td>{$query["salutation"]}</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>{$query["name"]}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{$query["email"]}</td>
                </tr>
                <tr>
                    <td>Mobile</td>
                    <td>{$query["mobile"]}</td>
                </tr>
                <tr>
                    <td>Content</td>
                    <td>{$query["message"]}</td>
                </tr>
                </table>
            ";
            $mail->send();
            
            $response["status"] = "success";
            $response["message"] = "";
        } catch (Exception $e) {
            $response["status"] = "fail";
            $response["message"] = $mail->ErrorInfo;
        }

        echo json_encode($response);
    }
    function GetMailConfig()
    {
        $mail = new PHPMailer(true);

        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
        $mail->isSMTP();
        $mail->SMTPAuth   = true;
        $mail->Host       = $this->_config["mail"]["host"];
        $mail->Username   = $this->_config["mail"]["username"];
        $mail->Password   = $this->_config["mail"]["password"];
        $mail->Port       = $this->_config["mail"]["port"];
        $mail->setFrom($this->_config["mail"]["from"]);
        $mail->addAddress($this->_config["mail"]["to"]);
        $mail->isHTML(true);

        return $mail;
    }
}