<?php

use Philo\Blade\Blade;
use Tightenco\Collect;

class ViewController
{    
    protected $_config;
    protected $_helper;
    function __construct()
    {
        $this->_config = include '../config.php';
        $this->_helper = new HelperController();
    }

    function view($view = null, $data = [], $mergeData = [])
    {
        $views = __Dir__ .'/'. '../resources/views';
        $cache = __Dir__ .'/'. '../resources/cache';
        $blade = new Blade($views, $cache);

        if(!$blade->view()->exists($view))
        {
            header("HTTP/1.0 404 Not Found");
            die;
        }

        $data["config"] = $this->_config;
        $data["categories"] = $this->_helper->GetCategories();
        $data["categoryTree"] = $this->_helper->GetCategorytree();

        return $blade->view()->make($view, $data, $mergeData);
    }

    function GetIndex()
    {   
        echo $this->view('home', [
        ]);
    }

    function GetAbout()
    {
        echo $this->view('about-us', [
        ]);
    }
    
    function GetBlogs()
    {
        $blogs = $this->_helper->GetBlogs();
        $blogs->transform(function($blog){
            if(count($blog["blog_images"]) > 0)
            {
                $blog["image"] = $blog["blog_images"][0]["image"];
            }
            return $blog;
        });

        $news = $blogs->where('group','!=','gallery');
        $gallery = $blogs->where('group','gallery');

        echo $this->view('news-gallery', [
            "news" => $news,
            "gallery" => $gallery
        ]);
    }

    function GetBlog($blogUrl)
    {
        $blogs = $this->_helper->GetBlogs();
        $blog = $blogs->firstWhere("url", $blogUrl);
        
        $blogTags = explode(',',$blog['tags']);

        echo $this->view('news-article', [
            "blog" => $blog,
            "blogTags" => $blogTags
        ]);
    }

    function GetPage($pageUrl)
    {
        echo $this->view($pageUrl, [
        ]);
    }

    function GetCategory($categoryCode,  $query = array())
    {
        return $this->GetSubCategory($categoryCode, null, $query);
    }

    function GetSubCategory($categoryCode, $subcategoryCode = null, $query = array())
    {
        $realSubCategoriesProduct=$subcategoryCode == null ? $this->GetRealSubCategory():null;

        $categoryTargetCode = $subcategoryCode == null ? $categoryCode : $subcategoryCode;
        $categories = $this->_helper->GetCategories();
        $products = $this->_helper->GetProducts();
        $productCategories = $this->_helper->GetProductCategories();
        
        $pageSort = array_key_exists("page_sort", $query) ? $query["page_sort"] : "name_asc";
        $pageLength = array_key_exists("page_length", $query) ? intval($query["page_length"]) : 20;
        $pageIndex = array_key_exists("page_index", $query) ? intval($query["page_index"]) : 1;

        $pageIndex = $pageIndex == 0 ? 1 : $pageIndex;
        $pageLength = $pageLength == 0 ? 20 : $pageLength;

        $params = [
            "page_sort" => $pageSort,
            "page_length" => $pageLength,
            "page_index" => $pageIndex
        ];

        $categories = $categories->filter(function($category) use ($categoryTargetCode){
            return $category['code'] === $categoryTargetCode;
        });

        // map product full info
        $categories->transform(function($category, $key) use ($products){
            $productIds = $category['productIds']->toArray();
            $category['products'] = $products->filter(function($product) use ($productIds) {
                return in_array($product['id'], $productIds);
            });
            return $category;
        });
        // add empty banner
        $categories->transform(function($category, $key){
            if(!array_key_exists("banners", $category)){
                $category['banners'] = [];
            }
            if(!array_key_exists("banner", $category)){
                if(count($category['banners']) > 0){
                    $category['banner'] = $category['banners'][0];
                }
                else{
                    $category['banner'] = null;
                }
            }
            return $category;
        });

        // sorting
        $categories->transform(function($category, $key) use ($pageSort) {
            switch($pageSort){
                case "price_asc":
                    $category['products'] = collect($category['products'])->sortBy('variant.price');
                    break;
                case "price_desc":
                    $category['products'] = collect($category['products'])->sortByDesc('variant.price');
                    break;
                case "name_asc":
                    $category['products'] = collect($category['products'])->sortBy('title');
                    break;
                case "name_desc":
                    $category['products'] = collect($category['products'])->sortByDesc('title');
                    break;
            }
            return $category;
        });

        $linkPrefix = "/our-product/$categoryCode" . ($subcategoryCode == null ? "" : "/$subcategoryCode"); 
        $sorting = [
            "links" => [
                ["code"=>"name_asc","label"=>"Name (A - Z)","url" => "$linkPrefix?page_sort=name_asc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
                ["code"=>"name_desc","label"=>"Name (Z - A)","url" => "$linkPrefix?page_sort=name_desc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
                ["code"=>"price_asc","label"=>"Name (Low to High)","url" => "$linkPrefix?page_sort=price_asc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
                ["code"=>"price_desc","label"=>"Price (High to Low)","url" => "$linkPrefix?page_sort=price_desc&page_length={$params['page_length']}&page_index={$params['page_index']}"],
            ],
            "current" => $params["page_sort"]
        ];

        // total product = 
        $productLength = $categories->reduce(function($carry, $item){
            return $carry + count($item["products"]);
        });
        $pageTotalLength = ceil($productLength/$pageLength);
        $pageTotalLength = $pageTotalLength == 0 ? 1 : $pageTotalLength;
        $links = collect(range(0,$pageTotalLength - 1))->map(function($item, $key) use ($linkPrefix,$categoryCode, $params){
            $query = array_merge(array(), $params); //clone
            $query["page_index"] = $key + 1;
            return "$linkPrefix?".http_build_query($query);
        });
        $pagination = [
            "links" => $links,
            "total" => $productLength,
            "current_page" => $pageIndex,
            "last_page" => $pageTotalLength,
        ];
        $prevPageIndex = $pageIndex > 1 ? $pageIndex - 1 : 1;
        $nextPageIndex = $pageTotalLength > $pageIndex ? $pageIndex + 1 : $pageTotalLength;
        $pagination["first_page_url"] = "$linkPrefix?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index=1";
        $pagination["last_page_url"] = "$linkPrefix?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index={$pageTotalLength}";
        $pagination["prev_page_url"] = "$linkPrefix?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index={$prevPageIndex}";
        $pagination["next_page_url"] = "$linkPrefix?page_sort={$params['page_sort']}&page_length={$params['page_length']}&page_index={$nextPageIndex}";

        // data length
        $categories->transform(function($category, $key) use ($pageLength, $pageIndex) {
            if(count($category["products"]) > 0){
                $products = collect($category['products']);
                $productPieces = $products->chunk($pageLength);
                $category['products'] = $productPieces[$pageIndex - 1];
            }
            return $category;
        });

        $categories = $categories->values();
        $category = ($categories->count() > 0) ? $categories[0] : null;
        
        

        echo $this->view('our-product', [
            'realSubCategoriesProduct' =>$realSubCategoriesProduct,
            'category' => $category,
            'pagination' => $pagination,
            'sorting' => $sorting,
            'categoryCode' => $categoryCode,
            'subcategoryCode' => $subcategoryCode,
        ]);
    }

    function GetRealSubCategory(){
        $productCategories = $this->_helper->GetProductCategories();
        $realSubCategories = $this->_helper->GetCategoriesChildren();
        $realSubCategoriesProduct = array();
        foreach($realSubCategories as $index => $subcategory){
            foreach($productCategories as $product){
                if($product['category_id']==$subcategory['id']){
                    array_push($realSubCategoriesProduct,array($product['product_id'],$subcategory['code']));
                }
            }
        }
        return $realSubCategoriesProduct;
    }
    
    function GetProduct($categoryCode,$subcategoryCode,$productUrl)
    {
        $categories = $this->_helper->GetCategories();
        $products = $this->_helper->GetProducts();
        
        $category = $categories->firstWhere('code',$categoryCode);
        $product = $products->firstWhere('url',$productUrl);
        ($subcategoryCode == $category['code'])?$subcategory=null:$subcategory = $categories->firstWhere('code',$subcategoryCode);
        echo $this->view('product-detail', [
            'product' => $product,
            'category' => $category,
            'subcategory' =>$subcategory,
        ]);
    }

    function GetSitemap(){
        $pages = $this->_helper->GetPages();
        $categoriesTree = $this->_helper->GetCategorytree();
        $categories = $this->_helper->GetCategories();
        $products = $this->_helper->GetProducts();
        $blogs = $this->_helper->GetBlogs()->where('group','!=','gallery');

        $categories->transform(function($category, $key) use ($categories){
            $category['parent_name'] = $categories->where('id',$category['parent_id'])->pluck('code')->first();
            return $category;
        });
        
        $categories->transform(function($category, $key) use ($products){
            $productIds = $category['productIds']->toArray();
            $category['products'] = $products->filter(function($product) use ($productIds) {
                return in_array($product['id'], $productIds);
            });
            return $category;
        });
        
        $sitemaps = collect();
        $sitemaps->push([
            "loc" => "https://progreen.com.my/",
            "lastmod" => date('Y-m-d',strtotime("-1 days")),
            "changefreq" => "weekly",
            "priority" => "0.8",
        ]);
        foreach($pages as $page){
            $sitemaps->push([
                "loc" => "https://progreen.com.my/".$page["url"],//$this->_config.$page["url"],
                "lastmod" => date('Y-m-d',strtotime("-1 days")),
                "changefreq" => "weekly",
                "priority" => "0.8",
            ]);
        }

        foreach($categoriesTree as $category){
            $sitemaps->push([
                "loc" => "https://progreen.com.my/our-product/".$category["code"],//$this->_config.$page["url"],
                "lastmod" => date('Y-m-d',strtotime("-1 days")),
                "changefreq" => "weekly",
                "priority" => "0.8",
            ]);
            if(isset($category['children'])){
                foreach($category['children'] as $index => $subcat){
                    $sitemaps->push([
                        "loc" => "https://progreen.com.my/our-product/".$category["code"].'/'.$subcat['code'],//$this->_config.$page["url"],
                        "lastmod" => date('Y-m-d',strtotime("-1 days")),
                        "changefreq" => "weekly",
                        "priority" => "0.8",
                    ]); 
                }
            }
        }

        $categories = $categories->where('parent_id','!=','');
        foreach($categories as $catProd){
            foreach($catProd['products'] as $product){
                $sitemaps->push([
                    "loc" => "https://progreen.com.my/our-product/".$catProd['parent_name'].'/'.$catProd["code"].'/'.$product['url'],//$this->_config.$page["url"],
                    "lastmod" => date('Y-m-d',strtotime("-1 days")),
                    "changefreq" => "weekly",
                    "priority" => "0.8",
                ]);
            }
        }

        foreach($blogs as $blog){
            $sitemaps->push([
                "loc" => "https://progreen.com.my/news-gallery/".$blog['url'],//$this->_config.$page["url"],
                "lastmod" => date('Y-m-d',strtotime("-1 days")),
                "changefreq" => "weekly",
                "priority" => "0.8",
            ]);
        }

        echo $this->view('sitemap', [
            'sitemaps' => $sitemaps
        ]);
    }

}