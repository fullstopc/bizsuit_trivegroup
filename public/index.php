<?php 

require_once '../vendor/autoload.php';
require_once '../controller/HelperController.php';
require_once '../controller/ViewController.php';

$config = include '../config.php';
$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) use ($config) {
    $r->addRoute('POST', rtrim($config["app_url"], '/') . '/import/data', 'HelperController/ImportData');
    $r->addRoute('POST', rtrim($config["app_url"], '/') . '/import/images', 'HelperController/ImportImages');
    $r->addRoute('POST', rtrim($config["app_url"], '/') . '/enquiry', 'HelperController/PostEnquiry');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/load/data', 'HelperController/GetData');

    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/our-product', 'ViewController/GetCategory');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/our-product/{categoryCode}', 'ViewController/GetCategory');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/our-product/{categoryCode}/{subcategoryCode}', 'ViewController/GetSubCategory');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/our-product/{categoryCode}/{subcategoryCode}/{productUrl}', 'ViewController/GetProduct');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/news-gallery', 'ViewController/GetBlogs');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/news-gallery/{blogUrl}', 'ViewController/GetBlog');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/about-us', 'ViewController/GetAbout');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/sitemap', 'ViewController/GetSitemap');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/{pageUrl}', 'ViewController/GetPage');
    $r->addRoute('GET', rtrim($config["app_url"], '/') . '/', 'ViewController/GetIndex');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];
$body = json_decode(file_get_contents('php://input'), true);
$query = $_REQUEST;

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
// echo json_encode([
//     $routeInfo,
//     $_REQUEST,
// ]); die;
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        $vars["query"] = $query;
        $vars["body"] = $body;
        
        // call_user_func_array($handler, $vars);
        list($class, $method) = explode("/", $handler, 2);
        call_user_func_array(array(new $class, $method), $vars);
        break;
}

?>

