<?php 
return [
    'app_url' => '/bizsuit_trivegroup/public/',
    'title_name' => 'TRIVE | PROPERTY GROUP',
    'company_name' => 'Trive Property Group Berhad.',
    'gakey' => '',
    'gtkey' => '',
    'contact' => [
        'office' => '+60(4)-403 1828',
        'fax' => '+60(4)-403 6828',
        'addr' => 'Lot 12, Phase 2, Kulim Hi-Tech Park,',
        'addr2' => 'Kulim',
        'addr3' => ' 09000, Kedah Darul Aman,',
        'addr4' => 'Malaysia.',
        'email' => 'info@trivegroup.com.my',
        'location' => 'https://www.google.com.my/maps/place/Eti+Tech+(M)+Sdn.+Bhd./@5.4225031,100.5825891,17z/data=!3m1!4b1!4m5!3m4!1s0x304acc69da78250f:0xe12c00ad01587770!8m2!3d5.4224978!4d100.5847778',
    ],
    'social' => [
        'facebook' => 'https://facebook.com/wltmarketing/',
        'linkedin' => '',
        'twitter' => 'https://twitter.com/ProgreenBhd',
        'youtube' => 'https://www.youtube.com/channel/UCBqLq4XayalxlBvGug5G08g?view_as=subscriber',
    ],
    'seo' => [
        'title' => 'TRIVE PROPERTY GROUP BERHAD',
        'type' => 'Website',
        'url' => '//trivepropertygroup.com.my',
        'image' => 'https://progreen.bizsuit.com.my/img/logo-og.png',
        'site_name' => 'TRIVE PROPERTY GROUP BERHAD',
    ],
    'ext_link' => [
        'ecomm' => 'http://progreenshop.xyz/'
    ],
    // 'mail' => [
    //     'from' => 'noreply@progreen.com.my',
    //     'to' => 'enquiry@progreen.com.my',
    //     'host' => 'mail.progreen.com.my',
    //     'port' => '587',
    //     'username' => 'noreply@progreen.com.my',
    //     'password' => 'Sunwayo0o!',
    // ],
];