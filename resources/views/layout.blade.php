<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    @yield("meta")
    <meta property="og:title" content="{{$config['seo']['title']}}" />
    <meta property="og:type" content="{{$config['seo']['type']}}" />
    <meta property="og:url" content="{{$config['seo']['url']}}" />
    <meta property="og:image" content="{{$config['seo']['image']}}" />
    <meta property="og:site_name" content="{{$config['seo']['site_name']}}" />
    
    <link rel="icon" href="{!!rtrim($config['app_url'], '/')!!}/img/triveLogo.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css2?family=Muli:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">


    <link rel="stylesheet" href="{!!rtrim($config["app_url"], '/')!!}/assets/css/app.css">
    
    <!-- Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', '{{$config["gakey"]}}', 'auto');
      ga('send', 'pageview');
      
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','{{$config["gtkey"]}}');</script>
    @yield("custom_style")
</head>

<body class="">
<div class="slider-wrapper main-slider">
    <div class="slider-item">
        <div class="slider-img" style="background-image:url('/img/progreen-background.jpg');"></div>
    </div>
</div>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{$config["gtkey"]}}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @yield('before_wrapper')
    <div class="wrapper" id="wrapper">
        @include("partial.header")

        <section id="content">
            @yield('content')
        </section>
        
        @include("partial.footer")
    </div>
    
    @yield('after_wrapper')
    @include("partial.script")
    <script>
    var initSubMenu = function(){
			var navTwo_Width = $(".nav-two").width();
			$(".nav-three").css({left:navTwo_Width,top:0,position:"absolute"});
			};
            $(".nav-one > li").hover(function() {
                $(this).find('.nav-two').stop(true, true).delay(200).fadeIn(300);
            }, function() {
                $(this).find('.nav-two').stop(true, true).delay(200).fadeOut(300);
            });
            $(".nav-two > li").hover(function() {
                $(this).find('.nav-three').stop(true, true).delay(200).fadeIn(300);
            }, function() {
                $(this).find('.nav-three').stop(true, true).delay(200).fadeOut(300);
            });
            $(function(){
				initSubMenu();
            });
        </script>
    @yield('javascript')
</body>
</html>
