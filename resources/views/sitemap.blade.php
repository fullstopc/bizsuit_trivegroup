<?php header('Content-type: text/xml'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($sitemaps as $sitemap)
        <url>
            <loc>{{ $sitemap['loc'] }}</loc>
            <lastmod>{{ $sitemap['lastmod'] }}</lastmod>
            <changefreq>{{ $sitemap['changefreq'] }}</changefreq>
            <priority>{{ $sitemap['priority'] }}</priority>
        </url>
    @endforeach
</urlset>
