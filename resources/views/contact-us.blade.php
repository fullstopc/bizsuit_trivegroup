@extends('layout')
@section('meta')
<title>Contact Us | {{$config["company_name"]}}</title>
<meta property="og:description" content="We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!" />
<meta name='description' content='We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!' />
<meta name='keywords' content='progreen, equipment & services, cleaning tools , cleaning chemical, hotel cleaning, industrial cleaning, cost saving cleaning' />
@endsection
@section('content')
<div class="subpage-banner">
    <div class="subpage-banner-item">
        <iframe src="{!!$config['contact']['location']!!}" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</div>
<div class="page-contactus">
    <div class="container">
    <h2 class="page-title text-secondary">Contact Us</h2>
    <br>
        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="contact-list">
                    <div class="contact-item">
                        <i class="fas fa-phone"></i>
                        <a href="tel:{!!$config["contact"]["office"]!!}">
                            <span>{!!$config['contact']['office']!!}</span>
                        </a>    
                    </div>
                    <div class="contact-item">
                        <i class="fas fa-envelope"></i>
                        <a href="tel:{!!$config["contact"]["email"]!!}">
                            <span>{!!$config['contact']['email']!!}</span>
                        </a> 
                    </div>
                    <div class="contact-item">
                        <i class="fas fa-map-marker"></i>
                        <a href="{!!$config["contact"]["location"]!!}" target="_blank">
                                    {!!$config["contact"]["addr"]!!}<br />
                                    {!!$config["contact"]["addr2"]!!}{!!$config["contact"]["addr3"]!!},<br />
                                    {!!$config["contact"]["addr4"]!!}
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8">
                <h3 class="font-weight-bold text-secondary">Enquiry Us</h3>
                <p>
                    We’d love to hear from you
                    Whether you have a question about pricing, advise, or anything else, 
                    our team is ready to answer all your questions
                    we will get back to you soon!
                </p>
                <form method="post" data-url="/enquiry" id="formContactUs">
                    <div class="alert alert-success d-none">
                        Email send successfully.
                    </div>
                    <div class="alert alert-danger d-none">
                        Something wrong happen,please try again later.
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="">Salutation</label>
                            <select name="salutation" class="form-control">
                                <option value='Mr'>Mr</option>
                                <option value='Mrs'>Mrs</option>
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="">Name</label>
                            <input required name="name" class="form-control" placeholder="Enter your name">
                        </div>
                    </div>   
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="">Email</label>
                            <input required name="email" type="email" class="form-control" placeholder="Enter your email">
                        </div>
                        <div class="form-group col">
                            <label for="">Mobile</label>
                            <input required name="mobile" class="form-control" placeholder="Enter your mobile">
                        </div>
                    </div>                     
                    <div class="form-group">
                        <label>Message</label>
                        <textarea required name="message" class="form-control" rows="3" placeholder="I would like to know about..."></textarea>
                    </div>
                        <input type="hidden" name="action" value="SendMail" />
                        <button type="submit" class="btn btn-secondary btn-send" href="#">
                            <span class="spinner spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <span class="">Submit</span>
                        </button>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="engaging-customer">
    @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>
   .btn-send{
    display:flex;
    align-items:center;
    justify-content:center;
    padding:.5rem 3rem;
}
.btn-send .spinner{
    margin-right:6px;
    display:none;
}
.btn-send-loading .spinner{
    display:block;
}
</style>
@endsection
@section('javascript')
<script>
        $(function(){
            $("#formContactUs").parsley({
                errorClass: 'is-invalid text-danger',
                errorsWrapper: '<span class="form-text text-danger"></span>',
                errorTemplate: '<span></span>',
                trigger: 'change',
                excluded:"input[type=button], input[type=reset]"
            }).on('form:submit', function() {
                var data = $("#formContactUs").serialize();
                var url = $("#formContactUs").data("url");

                $("#formContactUs :input").prop("disabled", true);
                $("#formContactUs .btn-send").prop("disabled", true).addClass("btn-send-loading");

                var promise = $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                }).then(function(response){
                    $("#formContactUs :input").prop("disabled", false);
                    $("#formContactUs .btn-send").prop("disabled", false).removeClass("btn-send-loading");
                    $("#formContactUs").trigger('reset');
                    response = JSON.parse(response);
                    if(response.status == 'success'){
                        $(".alert-success").removeClass("d-none");
                        setTimeout(function () {
                            $(".alert-success").addClass("d-none");
                        }, 5000);
                    }else{
                        $("#formContactUs :input").prop("disabled", false);
                        $("#formContactUs .btn-send").prop("disabled", false).removeClass("btn-send-loading");
                        $(".alert-danger").removeClass("d-none");
                    }
                    console.log(response);
                }, function(response){
                    console.log(response);
                    $("#formContactUs :input").prop("disabled", false);
                    $("#formContactUs .btn-send").prop("disabled", false).removeClass("btn-send-loading");
                    $(".alert-danger").removeClass("d-none");
                });
                return false;
            });
        });
        </script>
@endsection