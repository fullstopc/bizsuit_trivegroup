@extends('layout')
@section('meta')
<title>Covid-19 Disinfection Service | {{$config["company_name"]}}</title>
<meta property="og:description" content="Progreen Equipment and Services Sdn Bhd is offering disinfecting & sanitizing services due to the Covid-19 outbreak." />
<meta name='description' content='Progreen Equipment and Services Sdn Bhd is offering disinfecting & sanitizing services due to the Covid-19 outbreak.' />
<meta name='keywords' content='covid19, sanitize service, disinfect service, disinfectant spray, progreen, equipment & services, cleaning tools, emas award, cradle to cradle, ecolabel, kitchen hygiene, laundry, effective cleaning' />
@endsection
@section('content')
<div class="subpage-banner">
   <div class="subpage-banner-item" style="background:url(/img/progreen-disinfecting-service.jpg) center 30% / cover no-repeat;"></div>
</div>
<div class="page-aboutus">
   <div class="container mb-5">
      <h2 class="font-weight-bold text-secondary">Disinfection Service</h2>
      <br>
      <div class="row">
         <div class="col-12 col-sm-7 d-flex align-items-center text-justify">
            <p>
               <b class="text-primary">ProGreen Equipment & Services Sdn Bhd</b> is offering disinfecting services due to the <span class="text-danger font-weight-bold">Covid-19</span> outbreak.<br/>
               <br/>
               We use German Tana professional Apesin products to do the Disinfection. <br/>
               <br/>
               Apesin AP 100 Plus is a hospital grade disinfectant with HACCP certificate and can therefore be used in the food industry as well. <br/>
               <br/>
               The product is well tested and the laboratory tests speak for themselves.<br/>
               <br/>
               <a class="text-secondary" href="mailto:{{$config['contact']['email']}}"><u>Email Us</u></a> now immediately for further disinfection services!
               <br/>
               <br/>
               
               <small>*Prices on request<br/>
               *Availability subject to availability of raw materials due to market shortage</small>
      
            </p>            
         </div>
         <div class="col-12 col-sm-5 text-center">
            <img src="/img/logo.png" class="img-fluid w-75" alt="{!!$config['company_name']!!}" />
         </div>
      </div>
      <br/>
      <br/>
      <hr/>
      <h2 class="">Read More</h2>
      <br/>
      <div class="readmore row"> 
         <div class="col-sm-6 col-12">
            <a class="btn btn-secondary text-white w-100" href="/pdf/2017-VAH_Zertifikat_APESIN-AP-100-plus.pdf" target="_blank">VAH Certificate</a><br>
            <embed class="d-none d-sm-block" src="/pdf/2017-VAH_Zertifikat_APESIN-AP-100-plus.pdf" style="height:500px;width:100%;">
         </div>
         <div class="col-sm-6 col-12">
            <a class="btn btn-secondary text-white w-100" href="/pdf/40000275_APESIN-AP-100-plus_EN.pdf" target="_blank">HACCP Certificate</a><br>
            <embed class="d-none d-sm-block"  src="/pdf/40000275_APESIN-AP-100-plus_EN.pdf" style="height:500px;width:100%;">
         </div>
         <div class="col-sm-6 col-12">
            <a class="btn btn-secondary text-white w-100" href="/pdf/APESIN-range-against-Corona-virus_20200127.pdf" target="_blank">APESIN range against Corona-virus</a><br>
            <embed class="d-none d-sm-block"  src="/pdf/APESIN-range-against-Corona-virus_20200127.pdf" style="height:500px;width:100%;">
         </div>
         <div class="col-sm-6 col-12">
            <a class="btn btn-secondary text-white w-100" href="/pdf/APESIN-AP-Ingredient.png?t=23032020" target="_blank">Robert Koch Institute Listing</a><br>
            <img class="img-fluid d-none d-sm-block" src="/pdf/APESIN-AP-Ingredient.png" />
         </div>
         <div class="col-12">
         <a class="btn btn-secondary text-white m-3" href="/pdf/APESIN_AP_100_plus_Dr. Brill_EN 14476_Adenovirus_Poliovirus_MNV_voll_viruzid (1).pdf" target="_blank">Dr. Brill Lab Test 1</a>
         <a class="btn btn-secondary text-white m-3" href="/pdf/APESIN_AP_100_plus_EO_Brill_EN 13624_2010_lev_de_en.pdf" target="_blank">Dr. Brill Lab Test 2</a>
         <a class="btn btn-secondary text-white m-3" href="/pdf/APESIN_AP_100_plus_EO_Brill_EN 13727_2013_bact_low_load_en.pdf" target="_blank">Dr. Brill Lab Test 3</a>
         <a class="btn btn-secondary text-white m-3" href="/pdf/APESIN_AP_100_plus_EO_MikroLab_EN 14476_2007_Adeno_ED.pdf" target="_blank">MikroLab Info 1</a>
         <a class="btn btn-secondary text-white m-3" href="/pdf/APESIN_AP_100_plus_EO_MikroLab_EN 14476_2007_Polio_ED.pdf" target="_blank">MikroLab Info 2</a>
         <a class="btn btn-secondary text-white m-3" href="/pdf/APESIN_AP_100_plus_GA_Brill_EN 13727_2009_bact_de.pdf" target="_blank">Lab Tests</a>
         <a class="btn btn-secondary text-white m-3" href="/pdf/APESIN_AP_100_plus_PB_ihph_EN 13697_Clostridium difficile_D.pdf" target="_blank">Lab Tests</a>
         </div>
      </div>
      <div class="">
         <div class="d-flex justify-content-center align-items-center">
            <div class=" mb-5 px-4">
               <img src="/img/cradle.jpg" class="img-fluid" alt="{!!$config['company_name']!!}" />
            </div>
            <div class=" mb-5 px-4">
               <img src="/img/emas.png" class="img-fluid" alt="{!!$config['company_name']!!}" />
            </div>
            
            <div class=" mb-5 px-2">
               <img src="/img/ecolabel.jpg" class="img-fluid" alt="{!!$config['company_name']!!}" />
            </div>
         </div>
      </div>
</div>

<div class="engaging-customer">
    @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>
.readmore > div{
   padding-bottom:20px;
}   
</style>
@endsection
@section('javascript')
<script>

$(function(){
})
</script>
@endsection