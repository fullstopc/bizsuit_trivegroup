@extends('layout')
@section('meta')
<title>Annual Report | {{$config["company_name"]}}</title>
<meta property="og:description"
   content="We are award winners of EMAS, bringing a truly “green” environment friendly cleaning products into the South East Asian markets." />
<meta name='description'
   content='We are award winners of EMAS, bringing a truly “green” environment friendly cleaning products into the South East Asian markets.' />
<meta name='keywords'
   content='progreen, equipment & services, cleaning tools, emas award, cradle to cradle, ecolabel, kitchen hygiene, laundry, effective cleaning' />
@endsection
@section('content')


<style>
   .report-title {
      color: #c5a254;
      font-family: PT Serif;
      text-align: center;
   }

   .report-title-next {
      color: #002569;
      font-family: PT Serif;
      text-align: center;
      font-size: 30px;
      font-weight: 700;
   }

   .reports {
      border: 3px solid white;
      padding: 3px;
      margin: 10px;
   }

   .reports-inner-title {
      color: #c5a254;
      font-family: PT Serif;
      text-align: center;
      text-decoration:underline;
   }

   .reports-inner-percentage {
      color: #002569;
      font-family: PT Serif;
      text-align: center;
   }

   .section-annual-report{
      /* background-color: #F8F8F8; */
   }

   .reports-inner-content{
      color: #c5a254;
      font-family: Muli;
      text-align: center;
      font-size:12px;
   }

   .reports-inner-content1{
      color: #002569;
      font-family: Muli;
      text-align: center;
      font-size:13px;
   }

   .img-fluid-report{
       height:300px;
       width:210px;
   }

   }
</style>
<br /><br /><br />
<div class="section-annual-report">
   <div class="container py-5">
      <div class="row">
         <div class="col-12 col-lg-12">
            <div class="subpage-banner">
               <!-- <div class="subpage-banner-item" style="background:url(img/abstract-background.jpg) center / cover no-repeat; height:600px; padding-top:30px"> -->
               <h2 class="report-title">TRIVE</h2>
               <div class="report-title-next">Annual Report
                  <br />
                  <img class="img-fluid" src="img/triveLogo.png">
               </div>
               <br /><br />
               <!-- </div> -->
               <div class="row text-center">
                  <div class="col-4 p-0">
                     <div class="reports">
                        <div class="reports-inner">
                            <img class="img-fluid-report" src="img/2019.png">
                            <br />
                           <div class="reports-inner-title">Annual Report 2019</div>
                           <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-4 p-0">
                     <div class="reports">
                        <div class="reports-inner">
                            <img class="img-fluid-report" src="img/2018.png">
                                <br />
                            <div class="reports-inner-title">Annual Report 2018</div>
                            <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-4 p-0">
                     <div class="reports">
                        <div class="reports-inner">
                        <img class="img-fluid-report" src="img/2017.png">
                                <br />
                            <div class="reports-inner-title">Annual Report 2017</div>
                           <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-4 p-0">
                     <div class="reports">
                        <div class="reports-inner">
                        <img class="img-fluid-report" src="img/2016.png">
                                <br />
                            <div class="reports-inner-title">Annual Report 2016</div>
                           <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-4 p-0">
                     <div class="reports">
                        <div class="reports-inner">
                        <img class="img-fluid-report" src="img/2014.png">
                                <br />
                            <div class="reports-inner-title">Annual Report 2014</div>
                           <br />
                        </div>
                     </div>
                  </div>
                  <div class="col-4 p-0">
                     <div class="reports">
                        <div class="reports-inner">
                        <img class="img-fluid-report" src="img/2013.png">
                                <br />
                            <div class="reports-inner-title">Annual Report 2013</div>
                           <br />
                        </div>
                     </div>
                  </div>
                  
               </div>
            </div>

         </div>
      </div>

   </div>
</div>
<br /><br />
<div class="engaging-customer"
   style="background: url(img/cta-bg_02.jpg);background-repeat: no-repeat; background-size: cover; background-position: top bottom;">
   @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>

</style>
@endsection
@section('javascript')
<script>
   $(function () {})
</script>
@endsection