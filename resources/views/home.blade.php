@extends('layout')
@section('meta')
<title>{{$config["title_name"]}} </title>
<meta property="og:description"
    content="We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!" />
<meta name='description'
    content='We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!' />
<meta name='keywords'
    content='progreen, equipment & services, cleaning tools , cleaning chemical, hotel cleaning, industrial cleaning, cost saving cleaning' />
@endsection
@section('custom_style')

<style>
    .banner-box {
        border: 2px solid #ac893a;
        overflow: hidden;
        text-align: center;
        background: #fff;
        padding: 25px 25px;
        position: absolute;
        top: -100px;
        
    }

    .banner-box-splitter {
        width: 40px;
        height: 2px;
        background: #ac893a;
        margin: 20px auto;
    }

    .banner-box-header {
        display: flex;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding-top: 10px;
    }

    .banner-box-title {
        text-align: center;
        color: #002569;
        font-size: 20px;
        padding-top: 2px;
        padding-left: 6px;

    }

    .banner-box-icon {
        padding-right: 28px;
        padding-left: 28px;
    }

    .banner-box-link {
        text-decoration: underline;
    }

    .banner-box-content {
        color: #002569;
        text-align: center;

    }

    .content-splitter {
        width: 80px;
        height: 3px;
        background: #ac893a;
        float: left;
        margin: 10px;
    }



    .a {
        line-height: 1.5;
    }
</style>
<div id="fullpage">
    <div id="banner" class="block block-banner banner-main section section-banner">
        <div class="banner-list">
            <div class="banner-item banner-1" style="background: url(img/banner.jpg);background-repeat: no-repeat;
                background-size: cover;
                background-position: top bottom;">
                <div class="banner-item-inner">
                    <div class="banner-content">
                        <div class="m-sm-5 m-3">
                            <h3 class="font-weight-bold">
                                <h2 class> ECO-FRIENDLY</h2>
                                <h2 class>SOLAR ENERGY PIONEER </h2>
                            </h3>
                            <button onclick="window.location.href = '/about-us';"
                                class="btn btn-secondary align-self-start" style="color:#fff; border-radius:0px">read
                                more</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container"style="width:80%">
        <div class="row" style="position:relative">
            <div class="col-4">
                <div class="banner-box">
                    <h3 class="banner-box-header">
                        <img class="banner-box-icon" src="img/rnd-icon.png" />
                        <span class="banner-box-title">RESEARCH <br> & DEVELOPMENT</span>
                    </h3>
                    <div class="banner-box-splitter"></div>
                    <p class="banner-box-content"></p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a class="banner-box-link" href="#">See More</a>
                </div>
            </div>
            <div class="col-4">
                <div class="banner-box">
                    <h3 class="banner-box-header">
                        <img class="banner-box-icon" src="img/manufacture-icon.png" />
                        <span class="banner-box-title" style="left-padding:2px">MANUFACTURING</span>
                    </h3>
                    <div class="banner-box-splitter"></div>
                    <p class="banner-box-content">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <a class="banner-box-link" href="#">See More</a>
                </div>
            </div>
            <div class="col-4">
                <div class="banner-box">
                    <h3 class="banner-box-header">
                        <img class="banner-box-icon" src="img/invest-ion.png" />
                        <span class="banner-box-title">INVESTOR <br> RELATION</span>
                    </h3>
                    <div class="banner-box-splitter"></div>
                    <p class="banner-box-content"></p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a class="banner-box-link" href="#">See More</a>
                </div>
            </div>
        </div>
    </div>
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

    <div class="about">
        <div class="container" style="width:1300px">
            <div style="top-margin:40px">
                <div class="content-splitter"></div>
                <div class="section-about-title">ABOUT</div>
                <div class="section-about-header">
                    Solar Energy, <br />Earth Friendly Energy.
                </div>
            </div>
            <div class="section-about-paragraph">
                <p>
                    Trive Property Group Berhad, a public limited company listed on the Main Board of Bursa
                    Malaysia, is pioneering
                    the research & development
                    and manufacturing of green energy storage solutions and <br />battery related products that are
                    100% environmentally friendly.<br /><br />
                    Trive Property Group Berhad is a premier company producing Green Technology battery products
                    in <br />Malaysia and
                    exporting to international market.
                    Trive Property Group Berhad has a strong technical <br />core team supporting its R&O and
                    manufacturing at a fraction of the
                    cost of its main competitors.<br /><br />
                    Trive Property Group Berhad was formed in 2002 to assemble consumer, DEM/ODM battery packs,
                    it <br />has since evolved
                    to design and manufacturing of rechargeable Lithium battery
                    based on products <br />incuding systems for consumer, industrial and military applications.
                    Renewable Energy Storage <br />Systems,
                    Green Golf Cort Battery, Electric Vehicle Battery, Teko's backup Battery,
                    Standby Power<br /> Systems are among the new products commercialozed by the company.
                </p>
            </div>
        </div>
        <br /><br /><br /><br />
        <button onclick="window.location.href = '/about-us';" class="btn btn-secondary align-self-start"
                style="color:#fff;border-radius:0px;margin-left:500px">read more</button>
    </div>
    <br /><br /><br /><br /><br /><br />
    <div class="container">
        <div class="row">
            <div
                style="background: url(img/whyus-bg.jpg);background-repeat:no-repeat; background-size:cover; width:100%">
                <div class="whyus">
                    <br /><br />
                    <div class="whyus-splitter"></div>
                    <br /><br />
                    <div class="whyus-title">
                        10 years of Experience in Green Energy Storage Sector</div>
                    <div class="whyus-content">
                        Trive Property Group is fortunate to have assembled a team of industry experts
                        from various multinational
                        companies with
                        </br>
                        combinational know-how & skills that make Trive Property Group Berhad unique.
                    </div>
                    <br />
                    <div class="whyus-inner-icons">
                        <div class="col-sm-3 py-4 px-0">
                            <img src="{{rtrim($config["app_url"], '/')}}/img/why-us-icon01.png" />
                            <p class="whyus-icons-title">Lithium Rechargeble <br> Battery Technology </p>
                        </div>
                        <div class="col-sm-3 py-4 px-0">
                            <img src="{{rtrim($config["app_url"], '/')}}/img/why-us-icon02.png" />
                            <p class="whyus-icons-title">Proprietary <br> Intelligent BMS</p>
                        </div>
                        <div class="col-sm-3 py-4 px-0">
                            <img src="{{rtrim($config["app_url"], '/')}}/img/why-us-icon03.png" />
                            <p class="whyus-icons-title">Commercialise <br> Battery Packs</p>
                        </div>
                        <div class="col-sm-3 py-4 px-0">
                            <img src="{{rtrim($config["app_url"], '/')}}/img/why-us-icon04.png" />
                            <p class="whyus-icons-title">Competitive <br> Pricing</p>
                        </div>
                    </div>
                    <br />
                    <button onclick="window.location.href = '/about-us';" class="btn btn-secondary">read more</button>
                    <br /><br /><br /><br /><br /><br /><br />
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>

<div class="sustainability">
    <style>
        .pic1-blended {
            background-image: url(img/research.jpg);
            background-size: cover;
            background-color: #47477d;
            background-blend-mode: multiply;
            width: 100%;
            margin-bottom: 3px;
            height: 240px;
            display: flex;
            align-items: center;
            justify-content: center;

        }

        .pic2-blended {
            background-image: url(img/product.jpg);
            background-size: cover;
            background-color: #47477d;
            background-blend-mode: multiply;
            width: 100%;
            text-align: center;
            margin-bottom: 3px;
            height: 240px;
            display: flex;
            align-items: center;
            justify-content: center;

        }

        .pic3-blended {
            background-image: url(img/apability.jpg);
            background-size: cover;
            background-color: #47477d;
            background-blend-mode: multiply;
            width: 100%;
            text-align: center;
            margin-bottom: 3px;
            height: 240px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .pic4-blended {
            background-image: url(img/completedpro.jpg);
            background-size: cover;
            background-color: #47477d;
            background-blend-mode: multiply;
            width: 100%;
            text-align: center;
            margin-bottom: 3px;
            height: 240px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>
    <div class="container" style="width:80%">
        <div class="row">
            <div class="col-12 col-sm-5 p-0">
                <div class="content-splitter"></div>
                <p class="sustainability-header">SUSTAINABILITY</p>
                <h3 class="sustainability-title">
                    We can bring the <br />know-how gained to <br />compete on world stage.
                </h3>
                <br />
                <p class="sustainability-content">
                    As a pioneer in the green energy storage solution <br />provider, and with Malaysian Government
                    assistance we<br /> can bring
                    the know-how gained to a different level and <br />to compete on world stage.
                </p>
                <br /><br />
                <button onclick="window.location.href = '/about-us';" class="btn btn-secondary align-self-start"
                    style="color:#fff; border-radius:0px">read more</button>
            </div>

            <div class="col-12 col-sm-5 p-0">
                <div class="row">
                    <div class="col-6 p-0">
                        <div style="padding:3px">
                            <div class="pic1-blended">
                                <span style="color:white;font-weight:bold">RESEARCH</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 p-0">
                        <div style="padding:3px">
                            <div class="pic2-blended">
                                <span style="color:white;font-weight:bold">PRODUCT</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-6 p-0">
                        <div style="padding:3px">
                            <div class="pic3-blended">
                                <span style="color:white;font-weight:bold">CAPABILITIES</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 p-0">
                        <div style="padding:3px">
                            <div class="pic4-blended">
                                <span style="color:white;font-weight:bold">COMPLETED PROJECTS</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<div class="engaging-customer" style="background: url(img/cta-bg_02.jpg);background-repeat: no-repeat; background-size: cover; background-position: top bottom;">
    @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>
</style>
@endsection
@section('javascript')
<script>
    var InitSectionHeight = function () {
        var height = $(window).height();
        $(".section-fullheight").css('height', height + 'px');
    }
    var findBootstrapEnvironment = function () {
        let envs = ['xs', 'sm', 'md', 'lg', 'xl'];

        let el = document.createElement('div');
        document.body.appendChild(el);

        let curEnv = envs.shift();

        for (let env of envs.reverse()) {
            el.classList.add(`d-${env}-none`);

            if (window.getComputedStyle(el).display === 'none') {
                curEnv = env;
                break;
            }
        }

        document.body.removeChild(el);
        return curEnv;
    }
    var InitClientSlick = function () {
        var env = findBootstrapEnvironment();
        if (env == 'xs' || env == 'sm') {
            $(".client-list").not('.slick-initialized').slick({
                infinite: true,
                slidesToShow: 9,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [{
                    breakpoint: 789,
                    settings: {
                        slidesToShow: 3,
                    }
                }]
            });
        } else {
            $(".client-list").not('.slick-initialized').slick()
            $(".client-list").slick('unslick');
        }
    }

    $(function () {
        InitSectionHeight();
        InitClientSlick();

        $(window).resize(function () {
            InitSectionHeight();
            InitClientSlick();
        })


        $(".banner-list").slick({
            autoplay: true,
            autoplaySpeed: 5000,
            dots: true,
            arrows: false,
            arrows: false,
            arrows: false,
            arrows: false,
            arrows: false,
            fade: true,
            speed: 1000,
            slidesToShow: 1,
            pauseOnHover: false,
        });
    })
</script>
@endsection