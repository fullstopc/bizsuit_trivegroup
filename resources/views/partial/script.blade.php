<script src="{!!rtrim($config["app_url"], '/')!!}/assets/js/app.js"></script>
<script>
var InitPageChange = function(){
    var pathname = window.location.pathname || 'home';
    if(pathname == '/') pathname = 'home';

    $("#header .header-nav .nav-link").removeClass("active");
    $("#header .header-nav .nav-link").each(function(){
        var page = $(this).data("page");
        var level = $(this).data("level");
        var active = pathname.includes(page);

        if(active){
            $(this).addClass("active");
        }
    });

    $("#header .header-mobile-nav .nav-link").removeClass("active");
    $("#header .header-mobile-nav .nav-link").each(function(){
        var page = $(this).data("page");
        var level = $(this).data("level");
        var active = pathname.includes(page);

        if(active){
            $(this).addClass("active");
        }
    });
}
var InitScroll = function(){
    var lastScrollTop = 0;
    $(window).scroll(function () {
        var currentScrollTop = $(this).scrollTop();
        if(currentScrollTop == 0){
            $("body")
            .addClass("scrolled-top")
            .removeClass("scrolled-up")
            .removeClass("scrolled");
        }
        else if(lastScrollTop >= currentScrollTop){
            $("body")
            .addClass("scrolled-up")
            .removeClass("scrolled-top")
            .removeClass("scrolled");
        }
        else{
            $("body")
            .addClass("scrolled")
            .removeClass("scrolled-top")
            .removeClass("scrolled-up");
        }
        lastScrollTop = currentScrollTop;
    });
    $(window).scroll();
}
$(function(){
    InitPageChange();
    InitScroll();
    $(window).on('hashchange', function(e) {
        //InitHashPage();
    });
    $("#overlay").fadeOut(500);

    $(".hamburger-nav").click(function(){
        $(".header-hamburger").toggleClass('hamburger-active');
        $(".header-mobile-nav").toggleClass('header-mobile-nav-active');
    });

    // $(".banner-list").slick({
    //     autoplay: true,
    //     autoplaySpeed: 5000,
    //     dots: true,
    //     arrows: false, 
    //     fade: true,
    //     speed:1000,
    //     slidesToShow: 1,
    //     pauseOnHover:false,
    // });
});
</script>