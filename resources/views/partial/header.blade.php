<header id="header">
    <div class="header-topline"></div>
    <div class="header-inner p-10 px-10">
        
        <div class="header-contact col">
            <ul>
                <li class="header-contact-items">
                    <a>Schedule a consultation:</n></a>
                    <i class="fas fa-phone-square-alt" style="color:#002569;"></i>
                    <a class="pl-2" href="tel:{!!$config["contact"]["office"]!!}">{!!$config["contact"]["office"]!!}</a>
                    <i class="fas fa-fax" style="color:#002569;"></i>
                    <a class="pl-2" href="mailto:{!!$config["contact"]["fax"]!!}">{!!$config["contact"]["fax"]!!}</a>
                </li>
            </ul>
        </div>
        <div class="header-hamburger">
            <div class="hamburger-nav">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <div class="header-inner2 d-none d-sm-block d-lg-block d-xl-block">
        <div class="header-nav" style="padding-top:6px;">
            <ul class="nav-menu nav-one text-center">
                <li class="nav-item"><a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/home" data-page="home">HOME</a></li>
                <li class="nav-item"><a class="nav-link text-nowrap" href="{!! rtrim($config["app_url"], '/')!!}/about-us" data-page="about-us">ABOUT</a></li>
                <li class="nav-item"><a class="nav-link text-nowrap" href="{!! rtrim($config["app_url"], '/')!!}/corporate-structure" data-page="corporate-structure"> SOLUTION </a>
                </li>
                <div class="header-logo">
            <a href="{!! rtrim($config["app_url"], '/')!!}/annual-report">
                <img src="{!! rtrim($config["app_url"], '/')!!}/img/triveLogo.png" class="img-fluid" alt="Little Wonders"/>
            </a>
        </div>
                <li class="nav-item"><a class="nav-link text-nowrap" href="{!! rtrim($config["app_url"], '/')!!}/covid19-disinfection-service" data-page="covid19-disinfection-service">PROJECT</a></li>
                <li class="nav-item"><a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/news-gallery" data-page="news-gallery">INVESTOR</a></li>
                <li class="nav-item"><a class="nav-link text-nowrap" href="{!! rtrim($config["app_url"], '/')!!}/contact-us" data-page="contact-us">CONTACT</a></li>
            </ul>
        </div>
    </div>
    <div class="header-mobile-nav">
    <ul class="nav-menu nav-one">
            <li class="nav-item"><a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/home" data-page="home">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/about-us" data-page="about-us">About Us</a></li>
            <li class="nav-item"><a class="nav-link" data-page="corporate-structure">SOLUTION<i class="fas fa-caret-down"></i></a>
            <ul class="nav-menu nav-two m-0 pr-0">
                        @foreach($categoryTree as $rootCategory)
                            <li class="nav-item">
                                <a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/our-product/{{$rootCategory['code']}}">
                                    {{$rootCategory['name']}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
            </li>
            <li class="nav-item"><a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/covid19-disinfection-service" data-page="covid19-disinfection-service">Disinfection Service</a></li>
            <li class="nav-item"><a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/news-gallery" data-page="news-gallery">Gallery</a></li>
            <li class="nav-item"><a class="nav-link" href="{!! rtrim($config["app_url"], '/')!!}/contact-us" data-page="contact-us">Contact Us</a></li>
        </ul>
        <div class="header-contact">
            <div class="header-contact-item contact-mobile">
                <a href="tel:<?php echo str_replace(" ","", $config["contact"]["office"]) ?>">
                    <?php echo $config["contact"]["office"] ?>
                </a>
            </div>
            <div class="header-contact-item contact-addr">
                <a href="<?php echo $config["contact"]["location"] ?>" target="_blank">
                    <?php echo $config["contact"]["addr"]; ?><br />
                    <?php echo $config["contact"]["addr2"]; ?><br />
                    <?php echo $config["contact"]["addr3"]; ?>
                </a>
            </div>
            <div class="header-contact-item contact-email">
                <a href="mailto:<?php echo $config["contact"]["email"] ?>">
                    <?php echo $config["contact"]["email"] ?>
                </a>
            </div>
        </div>
        <div class="header-socialmedia">
            <div class="socialmedia-item logo-fb">
                <a href="<?php echo $config["social"]["facebook"] ?>">
                    <img src="<?php echo $config["app_url"] ?>/img/social/facebook.png" class="img-fluid bg-light rounded-circle" alt="Progreen Facebook Official"/>
                </a>
            </div>
            <div class="socialmedia-item logo-insta">
                <a href="<?php echo $config["social"]["instagram"] ?>">
                    <img src="<?php echo $config["app_url"] ?>/img/social/instagram.png" class="img-fluid bg-light rounded-circle" alt="Progreen Instagram Official"/>
                </a>
            </div>
            <div class="socialmedia-item logo-twit">
                <a href="<?php echo $config["social"]["twitter"] ?>">
                    <img src="<?php echo $config["app_url"] ?>/img/social/twitter.png" class="img-fluid bg-light rounded-circle" alt="Progreen Twitter Official"/>
                </a>
            </div>
            <div class="socialmedia-item logo-yt">
                <a href="<?php echo $config["social"]["youtube"] ?>">
                    <img src="<?php echo $config["app_url"] ?>/img/social/youtube.png" class="img-fluid bg-light rounded-circle" alt="Progreen Youtube Channel"/>
                </a>
            </div>
        </div>
    </div>
</div>
</header>