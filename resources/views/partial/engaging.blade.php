<div class="container">
    <div class="row-justify-content-end" style="float:right; font-size:20px">
        <div class="col-12" style="padding-left:0px">
            <p class="text-secondary">Partner with Trive Property Group Berhad to reach your sustainability goals!</p>
            <p>Turn the challenge of energy into an opportunitiy for your business.<p>
        </div>
        <br>
            <a class="btn btn-secondary align-self-start"
                style="color:#fff; float:left; border-radius:0px; margin-right:486px"
                href="tel:<?php echo str_replace(" ","", $config["contact"]["office"]) ?>">
                Suscribe a consultation
            </a>
    </div>
</div>