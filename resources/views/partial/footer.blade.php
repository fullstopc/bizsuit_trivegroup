
<footer id="footer">
    <div class='footer-inner-container' style="width:80%">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h2 class="footer-quicklink-header">Get In Touch</h2>
                </br></br></br>

                <div class="row" style="flex:1">
                    <div class="col-12 col-sm-6">
                        <h4 class="footer-quicklink-title">EMAIL</h4>
                        <a class="footer-email" href="mailto:{!!$config["contact"]["email"]!!}">{!!$config["contact"]["email"]!!}</a>
                        </br></br></br>
                        <h4 class="footer-quicklink-title">CONTACT</h4>
                        <p class="footer-tel-fax">Tel: <a href="tel:{!!$config["contact"]["office"]!!}">{!!$config["contact"]["office"]!!}</a></p>
                        <p class="footer-tel-fax">Fax: <a href="tel:{!!$config["contact"]["fax"]!!}">{!!$config["contact"]["fax"]!!}</a></p>
                    </div>

                    <div class="col-12 col-sm-6">
                        <h4 class="footer-quicklink-title">ADDRESS</h4>
                        <a class="footer-address" href="{!!$config["contact"]["location"]!!}" target="_blank">{!!$config["contact"]["addr"]!!}<br />{!!$config["contact"]["addr2"]!!}{!!$config["contact"]["addr3"]!!}<br />{!!$config["contact"]["addr4"]!!}</a>
                        </br></br></br>
                        <h4 class="footer-quicklink-title">SOCIAL MEDIA</h4>
                        <div class="social-icon-facebook" target="_blank" href="{!!$config["social"]["facebook"]!!}" >
                            <i class="fab fa-facebook-square"></i></div>
                        <div class="social-icon-twitter" target="_blank" href="{!!$config["social"]["twitter"]!!}">
                            <i class="fab fa-twitter-square"></i></div>
                        <div class="social-icon-linkedin" target="_blank" href="{!!$config["social"]["linkedin"]!!}">
                            <i class="fab fa-linkedin"></i></div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6">
                <h2 class="footer-quicklink-header">Quick Link</h2>
                </br></br></br>
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <h4 class="footer-quicklink-title">ABOUT</h4>
                        <a class="footer-inner-link" href="#">Executive Summary</a>
                        <br />
                        <a class="footer-inner-link" href="#">Corporate Structure</a>
                        <br />
                        <a class="footer-inner-link" href="#">Introduction of Property Development</a>
                        <br />
                        <a class="footer-inner-link" href="#">Our Competitiveness</a>
                        <br />
                        <a class="footer-inner-link" href="#">Product Capabilities</a>
                        <br /><br /><br />
                        <h4 class="footer-quicklink-title">SOLUTION</h4>
                        <a class="footer-inner-link" href="#">Research</a>
                        <br />
                        <a class="footer-inner-link" href="#">Products</a>
                        <br />
                        <a class="footer-inner-link" href="#">Capabilities</a>
                    </div>

                    <div class="col-12 col-sm-6">
                        <h4 class="footer-quicklink-title">INVESTOR</h4>
                        <a class="footer-inner-link" href="#">Board Charter</a>
                        <br />
                        <a class="footer-inner-link" href="#">Code of Business Conducts</a>
                        <br />
                        <a class="footer-inner-link" href="#">Corporate Disclosure Policy</a>
                        <br />
                        <a class="footer-inner-link" href="#">Audit Committee</a>
                        <br />
                        <a class="footer-inner-link" href="#">Nominating Committee</a>
                        <br />
                        <a class="footer-inner-link" href="#">Remuneration Committee</a>
                        <br />
                        <a class="footer-inner-link" href="#">Risk Management Committee</a>
                        <br />
                        <a class="footer-inner-link" href="#">Whistleblowing Policy</a>
                        <br />
                        <a class="footer-inner-link" href="#">Annual Report</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        COPYRIGHT © 2020 {{$config["company_name"]}} ALL RIGHT RESERVED.
    </div>
</footer>