@extends('layout')
@section('meta')
<title>{{$category['name']}} | {{$config["title_name"]}}</title>
<meta property="og:description" content="We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!" />
<meta name='description' content='We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!' />
<meta name='keywords' content='progreen, equipment & services, cleaning tools , cleaning chemical, hotel cleaning, industrial cleaning, cost saving cleaning, tana chemical, green care professional' />
@endsection
@section('content')
<div class="subpage-banner">
    <div class="subpage-banner-item" style="background:linear-gradient(to right ,rgba(255,255,255,.5) 0% ,rgba(255,255,255,.5) 100%),url(/img/household-cleaning.jpg) center top / cover no-repeat;"></div>
</div>
<div class="page-ourproduct">
    <div class="container text-center">
        <h2 class="font-weight-bold text-secondary text-left">{{$category['name']}}</h2>
        <br>
        <ul class="nav nav-tabs">
            @foreach($categoryTree as $categoryNode)
                @if($categoryNode['code'] == $categoryCode)
                    @if(!empty($categoryNode['children']))
                        @foreach($categoryNode['children'] as $categorySubNode)
                        <li class="nav-item">
                            <a 
                            class="nav-link {!! $categorySubNode['code'] == $category['code'] ? "active" : "" !!}" 
                            href="{!!rtrim($config["app_url"], '/')!!}/our-product/{{$categoryNode['code']}}/{{$categorySubNode['code']}}">
                                {{$categorySubNode['name']}}
                            </a>
                        </li>
                        @endforeach
                    @endif
                    @break
                @endif
            @endforeach
        </ul>
        <br/>
        <div class="product-list h-100">
            <div class="container">
                @if($category['products']->count() == 0)
                    <div class="table-bordered p-5 d-flex justify-content-center align-items-center"><h2 class="text-muted">Stay tuned for more products!</h2></div>
                @endif
                <div class="row" style="margin-left:-20px; margin-right:-20px;">
                    @foreach($category['products'] as $product)  

                    <div class="col-lg-3 col-md-4 col-6 p-0"> 
                        <div class="product-item">
                        
                        @if ($realSubCategoriesProduct == null)
                            <a href="{!!rtrim($config["app_url"], '/')!!}/our-product/{!!$categoryCode!!}/{!!$category['code']!!}/{!!$product['url']!!}">
                        @else
                                
                            @foreach($realSubCategoriesProduct as $realsubcategory)
                                @if ($product['id']==$realsubcategory[0])
                                <a href="{!!rtrim($config["app_url"], '/')!!}/our-product/{!!$categoryCode!!}/{!!$realsubcategory[1]!!}/{!!$product['url']!!}">

                                @endif
                            @endforeach
                        @endif
                    
                                <div class="card h-100">
                                    <div class="card-img">
                                        <img class="card-img-top p-sm-5" src="{!!rtrim($config["app_url"], '/')!!}/img/product/{{$product['id']}}/{{$product['images'][0]['image']}}">
                                    </div>
                                    <div class="card-body text-left">
                                        <h5 class="card-title m-0">{{$product['title']}}</h5>
                                        <p class="card-text">{{$category['name']}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    @endforeach
                </div>
            </div>
        </div>
        <div class='row'>
            <div class="col">
                <div class="d-flex align-items-center justify-content-end mt-2 mb-2">
                    <nav>
                        <ul class="pagination">
                            <li class="page-item {{$pagination["current_page"] == 1 ? "disabled" : "" }}"><a class="page-link" href="{{$pagination["prev_page_url"]}}">Previous</a></li>
                            <?php foreach($pagination["links"] as $index => $link) { ?>
                            <li class="page-item {{$pagination["current_page"] == ($index + 1) ? "active" : "" }}"><a class="page-link" href="{{$pagination["current_page"] == ($index + 1) ? "#" : $link }}">{{$index + 1}}</a></li>
                            <?php } ?>
                            <li class="page-item {{$pagination["current_page"] == $pagination["last_page"] ? "disabled" : "" }}"><a class="page-link" href="{{$pagination["next_page_url"]}}">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="engaging-customer">
    @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>
   
</style>
@endsection
@section('javascript')
<script>

$(function(){
})
</script>
@endsection