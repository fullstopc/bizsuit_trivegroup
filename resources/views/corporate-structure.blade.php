@extends('layout')
@section('meta')
<title>Corporate Structure | {{$config["company_name"]}}</title>
<meta property="og:description"
   content="We are award winners of EMAS, bringing a truly “green” environment friendly cleaning products into the South East Asian markets." />
<meta name='description'
   content='We are award winners of EMAS, bringing a truly “green” environment friendly cleaning products into the South East Asian markets.' />
<meta name='keywords'
   content='progreen, equipment & services, cleaning tools, emas award, cradle to cradle, ecolabel, kitchen hygiene, laundry, effective cleaning' />
@endsection
@section('content')


<style>
   .corporate-title {
      color: #c5a254;
      font-family: PT Serif;
      text-align: center;
   }

   .corporate-title-next {
      color: #002569;
      font-family: PT Serif;
      text-align: center;
      font-size: 30px;
      font-weight: 700;
   }

   .members {
      border: 3px solid #002569;
      padding: 3px;
      margin: 10px;
   }

   .members-inner-title {
      color: #c5a254;
      font-family: PT Serif;
      text-align: center;
   }

   .members-inner-percentage {
      color: #002569;
      font-family: PT Serif;
      text-align: center;
   }

   .section-corporate-structure{
      /* background-color: #F8F8F8; */
   }

   .members-inner-content{
      color: #c5a254;
      font-family: Muli;
      text-align: center;
      font-size:12px;
   }

   .members-inner-content1{
      color: #002569;
      font-family: Muli;
      text-align: center;
      font-size:13px;

   }

   }
</style>
<br /><br /><br />
<div class="section-corporate-structure">
   <div class="container py-5">
      <div class="row">
         <div class="col-12 col-lg-12">
            <div class="subpage-banner">
               <!-- <div class="subpage-banner-item" style="background:url(img/abstract-background.jpg) center / cover no-repeat; height:600px; padding-top:30px"> -->
               <h2 class="corporate-title">TRIVE</h2>
               <div class="corporate-title-next">Corporate Structure
                  <br />
                  <img class="img-fluid" src="img/triveLogo.png">
               </div>
               <br /><br />
               <!-- </div> -->
               <div class="row text-center">
                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">ETI Tech (M) Sdn Bhd</div>
                           <div class="members-inner-percentage">100%</div>
                           <div class="members-inner-content">Date of Incorporation: 30.7.2002</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Trading, design & marketing of battery management system for rechargeable energy storage solutions.</div>
                        </div>
                     </div>
                  </div>

                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">Proper Methods Sdn Bhd</div>
                           <div class="members-inner-percentage">100%</div>
                           <div class="members-inner-content">Date of Incorporation: 11.11.2013</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Property development, construction and property investment sectors.</div>
                        </div>
                     </div>
                  </div>

                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">Pakadiri Sdn Bhd</div>
                           <div class="members-inner-percentage">100%</div>
                           <div class="members-inner-content">Date of Incorporation: 12.03.1986</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Housing Developer</div>
                           <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">Trive Property Sdn Bhd</div>
                           <div class="members-inner-percentage">100%</div>
                           <div class="members-inner-content">Date of Incorporation: 08.06.2015</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Dormant</div>
                           <br />
                        </div>
                     </div>
                  </div>
               </div>

               <div class="row text-center">
                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">ETI Tech International Sdn Bhd</div>
                           <div class="members-inner-percentage">100%</div>
                           <div class="members-inner-content">Date of Incorporation: 14.03.2006</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Dormant</div>
                           <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">ETI Tech Homes Sdn Bhd</div>
                           <div class="members-inner-percentage">100%</div>
                           <div class="members-inner-content">Date of Incorporation: 03.01.2012</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Dormant</div>
                           <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">Daima Fujing New Technology Sdn Bhd</div>
                           <div class="members-inner-percentage">100%</div>
                           <div class="members-inner-content">Date of Incorporation: 19.10.2015</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Dormant</div>
                           <br />
                        </div>
                     </div>
                  </div>

                  <div class="col-3 col-sm-3 p-0">
                     <div class="members">
                        <div class="members-inner">
                           <div class="members-inner-title">Avenue Escapade Sdn Bhd</div>
                           <div class="members-inner-percentage">60%</div>
                           <div class="members-inner-content">Date of Incorporation: 12.02.2015</div>
                           <div class="members-inner-content1">Pricipal Activities: <br />Investment Holding</div>
                           <br />
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>

   </div>
</div>
<br /><br /><br />

<br /><br /><br />
<div class="engaging-customer"
   style="background: url(img/cta-bg_02.jpg);background-repeat: no-repeat; background-size: cover; background-position: top bottom;">
   @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>

</style>
@endsection
@section('javascript')
<script>
   $(function () {})
</script>
@endsection