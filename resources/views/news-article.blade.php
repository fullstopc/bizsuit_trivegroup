@extends('layout')
@section('meta')
<title>{!!$blog['meta_title']!!} | {{$config["title_name"]}}</title>
<meta property="og:description" content="{!!$blog['meta_desc']!!}" />
<meta name='description' content='{!!$blog['meta_desc']!!}' />
<meta name='keywords' content='{!!$blog['meta_keywords']!!}' />
@endsection
@section('content')
<div class="blog-content container mt-5 mb-5">
    <hr />
    <h1 class="text-center pt-3">{!!$blog['title']!!}</h1>
    {!!str_replace("/media/website/uploads/","/img/uploads/",$blog['body'])!!}
    <hr />
    <div class="blog-tags">
        <h4>
        @foreach($blogTags as $tags)
            <span class="badge badge-lg badge-primary">{{$tags}}</span>
        @endforeach
        </h4>
    </div>
</div>
<div class="engaging-customer">
    @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>
    
</style>
@endsection
@section('javascript')
<script>
</script>
@endsection