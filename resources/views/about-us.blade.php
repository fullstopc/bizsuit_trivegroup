@extends('layout')
@section('meta')
<title>About Us | {{$config["company_name"]}}</title>
<meta property="og:description"
   content="We are award winners of EMAS, bringing a truly “green” environment friendly cleaning products into the South East Asian markets." />
<meta name='description'
   content='We are award winners of EMAS, bringing a truly “green” environment friendly cleaning products into the South East Asian markets.' />
<meta name='keywords'
   content='progreen, equipment & services, cleaning tools, emas award, cradle to cradle, ecolabel, kitchen hygiene, laundry, effective cleaning' />
@endsection
@section('content')
<style>
   .text-block {
      bottom: 20px;
      right: 20px;
      background-color: transparent;
      color: #c5a254;
      padding-left: 20px;
      padding-right: 20px;
      font-family: PT Serif;
      font-size: 20px;
      text-align: center;
      -webkit-text-stroke-width: thin;
   }

   .text-block2 {
      bottom: 20px;
      right: 20px;
      background-color: transparent;
      color: #002569;
      padding-left: 20px;
      padding-right: 20px;
      font-family: PT Serif;
      font-size: 25px;
      text-align: center;
      -webkit-text-stroke-width: medium;
   }

   .text-container {
      font-family: Muli;
      text-align: center;
      font-weight: 650;
   }

   .text-container1 {
      background-color: #002569;
   }

   .company-name {
      color: #c5a254;
   }

   .text-vision {
      bottom: 20px;
      right: 20px;
      background-color: transparent;
      color: #c5a254;
      padding-left: 20px;
      padding-right: 20px;
      font-family: PT Serif;
      font-size: 20px;
      text-align: center;
      -webkit-text-stroke-width: thin;
   }

   .vision-mission-content {
      color: white;
      font-family: Muli;
      text-align: center;
   }

   .text-trive {
      padding-top: 30px;
      font-size: 25px;
      font-family: PT Serif;
      color: #c5a254;
   }

   .text-investor {
      font-family: PT Serif;
      font-size: 30px;
      color: white;
   }

   .text-container-investor {
      font-family: Muli;
      color: #002569;
   }

   .text-investor-content {
      font-family: Muli;
      font-size: 20px;
      font-weight: 500;
   }
   
</style>
<div class="subpage-banner">
   <div class="subpage-banner-item" style="background:url(img/blue.jpg) bottom left / cover no-repeat">
      <div class="text-block">OUR COMPANY</div>
      <div class="text-block2">TRIVE - Property Group Berhad</div>
      <br />
      <div class="text-container">
         <b class="company-name">TRIVE Property Group Berhad</b>, a public limited company listed on the Main Board of
         Bursa Malaysia, is pioneering
         <br />the research & development and manufacturing of green energy storage solutions and battery related
         products
         that are <br />100% environmentally friendly.
      </div>
   </div>
</div>
<div class="page-aboutus">
   <div class="container mb-5">
      <br>
      <div class="row">
         <div class="col-12 col-sm-5" style="padding-left:80px">
            <div class="text-container1">
               <br />
               <div class="text-vision">Company Vision</div>
               <br />
               <p class="vision-mission-content">
                  “ to pioneer the research, develop properties and providing the green energy storage solutions and
                  battery energy <br />related products that are 100% environment friendly ”
               </p>
               <br />
            </div>
            <br />
            <div class="text-container1">
               <br />
               <div class="text-vision">Company Mission</div>
               <br />
               <p class="vision-mission-content">
                  “ to produce 100% environment friendly products and fulfil customers’ needs by providing green energy
                  <br />storage solutions & battery related products ”
               </p>
               <br />
            </div>
         </div>
         <div class="col-12 col-sm-6 text-center">
            <img src="img/trive-banner-img.jpg" style="height:400px" class="img-fluid"
               alt="{!!$config['company_name']!!}" />
         </div>
      </div>
      <br /><br /><br /><br />
      <div class="row">
         <div class="col-12 col-sm-12 text-center">
            <div class="text-container1">
               <div class="text-trive">TRIVE</div>
               <p class="text-investor">Investor Relations</p>
               <br />
            </div>
         </div>
      </div>
      <br /><br />
      <div class="row">
         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <img src="img/financial.jpg" class="img-fluid" alt="{!!$config['company_name']!!}" />
            <br />
         </div>

         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <div class="text-container-investor">
               <div class="text-investor-content">Financial Reports</div>
               <p style="color:black">Financial reports of years</p>
               <button onclick="window.location.href = '/about-us';" class="btn btn-secondary align-self-start"
                  style="color:#fff; border-radius:0px">read
                  more</button>
               <br />
            </div>
         </div>

         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <div class="text-container-investor">
               <img src="img/report.png" class="img-fluid" alt="{!!$config['company_name']!!}" />
               <br />
            </div>
         </div>

         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <div class="text-container-investor">
               <div class="text-investor-content">Annual Reports</div>
               <p style="color:black">Annual reports of years</p>
               <button onclick="window.location.href = '/about-us';" class="btn btn-secondary align-self-start"
                  style="color:#fff; border-radius:0px">read
                  more</button>
               <br />
            </div>
         </div>

         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <div class="text-container-investor">
               <img src="img/trive-banner-img.jpg" class="img-fluid" alt="{!!$config['company_name']!!}" />
               <br />
            </div>
         </div>

         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <div class="text-container-investor">
               <div class="text-investor-content">Board Charter</div>
               <p style="color:black">Core structure of TRIVE</p>
               <button onclick="window.location.href = '/about-us';" class="btn btn-secondary align-self-start"
                  style="color:#fff; border-radius:0px">read
                  more</button>
               <br />
            </div>
         </div>

         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <div class="text-container-investor">
               <img src="img/factory.png" class="img-fluid" alt="{!!$config['company_name']!!}" />
               <br />
            </div>
         </div>

         <div class="col-12 col-sm-3 d-flex align-items-center text-justify">
            <div class="text-container-investor">
               <div class="text-investor-content">Company Annoucements</div>
               <p style="color:black">Media & company annoucements</p>
               <button onclick="window.location.href = '/about-us';" class="btn btn-secondary align-self-start"
                  style="color:#fff; border-radius:0px">read
                  more</button>
               <br />
            </div>
         </div>
      </div>
      <br /><br /><br />

      <div class="row">
         <div class="col-12 col-sm-12">
            <div class="subpage-banner">
               <div class="subpage-banner-item"
                  style="background:url(img/blue.jpg) bottom left / cover no-repeat; height:180px; padding-top:30px">
                  <div class="text-block2" style="text-align-last:right">Get to know us more?</div>
                  <br />
                  <button onclick="window.location.href = '/about-us';" class="btn btn-secondary align-self-start"
                     style="color:#fff; border-radius:0px; float:right; margin-right:150px">read
                     more</button>
               </div>

            </div>
         </div>
      </div>
      <br /><br /><br />

      <div class="engaging-customer"
         style="background: url(img/cta-bg_02.jpg);background-repeat: no-repeat; background-size: cover; background-position: top bottom;">
         @include('partial.engaging')
      </div>
      @endsection
      @section('custom_style')
      <style>

      </style>
      @endsection
      @section('javascript')
      <script>
         $(function () {})
      </script>
      @endsection