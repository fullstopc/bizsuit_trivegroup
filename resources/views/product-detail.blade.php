@extends('layout')
@section('meta')
<title>{{$product['title']}} | {{$config["title_name"]}}</title>
<meta property="og:description" content="We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!" />
<meta name='description' content='We are specialized in supplying effective Cleaning Chemical in terms of effective way. Make a Visit with us at Johor Bahru now!' />
<meta name='keywords' content='progreen, equipment & services, cleaning tools , cleaning chemical, hotel cleaning, industrial cleaning, cost saving cleaning, tana chemical, green care professional' />
@endsection
@section('content')
<div class="page-product-detail">
    <div class="container">
    <h6><small><a href="/home">Home</a> / <a href="{!!rtrim($config["app_url"], '/')!!}/our-product/{{$category['code']}}">{{$category['name']}}</a> / <a href="{!!rtrim($config["app_url"], '/')!!}/our-product/{{$category['code']}}/{{$subcategory['code']}}">{{$subcategory['name']}}</a> / {{$product['title']}}</small></h6>

        <div class="row mt-5">
            <div class="col-12 col-md-4">
                    <div class="product-img">
                    @foreach($product['images'] as $img)
                        <div class="product-img-container ">
                            <img class="h-100 p-5" src="{!!rtrim($config["app_url"], '/')!!}/img/product/{{$product['id']}}/{{$img['image']}}"/>
                        </div>
                    @endforeach
                    </div>
                    <div class="product-thumb mt-2">
                    @foreach($product['images'] as $img)
                        <div>
                            <img class="img-fluid" src="{!!rtrim($config["app_url"], '/')!!}/img/product/{{$product['id']}}/{{$img['image']}}"/>
                        </div>
                    @endforeach
                    </div>
            </div>
            <div class="col-12 col-md-8 ">
                <div class="text-container">
                    <h2 class="text-primary">{{$product['title']}}</h2>
                    {!!$product['desc']!!}
                    <div class="pt-5">
                    @foreach($product['attachments'] as $attachment)
                        <a class="btn btn-secondary text-white" href="{!! rtrim($config["app_url"], '/')!!}/img/product/{{$product['id']}}/{{$attachment['properties']['filename']}}" target="_blank">{{$attachment['properties']['title']}}</a>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="engaging-customer">
    @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>
    
</style>
@endsection
@section('javascript')
<script>
    $(function () {
        // $("#GalleryList").unitegallery({
        //     gallery_theme: "tiles",
        //     tile_enable_textpanel: true,
        //     tile_textpanel_title_text_align: "center",
        //     tiles_type: "justified",
		// 	tiles_justified_row_height: 200,
        // })
        $(".product-img").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.product-thumb'	
        });
        $(".product-thumb").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.product-img',
            dots: true,
            centerMode: true,
            focusOnSelect: true
        });

        var $grid = null;
        $(window).on("load", function() {
            $grid = $('.gallery-list').isotope({
                itemSelector: '.gallery-item',
                columnWidth: '.gallery-item',
                percentPosition: true
            });

            
        });

        $(".btn-filter-gallery").click(function () {
            var category = $(this).data('category');
            $grid.isotope({
                filter: "[data-category='" + category + "']"
            });
        });
        
        $(".btn-filter-all").click(function () {
            $grid.isotope({ filter: "*" });
        });

        $(".gallery-item").SmartPhoto({
            useOrientationApi:false,
            useHistoryApi: false,
        });
    })
</script>
<script>
$(function(){
})
</script>
@endsection