@extends('layout')
@section('meta')
<title>Property Management | {{$config["title_name"]}}</title>
<meta property="og:description"
    content="We are specialized in supplying effective Cleaning Chemical in terms of effective way. Checkout our News & Product Effectiveness now!" />
<meta name='description'
    content='We are specialized in supplying effective Cleaning Chemical in terms of effective way. Checkout our News & Product Effectiveness now!' />
<meta name='keywords'
    content='progreen, equipment & services, cleaning tools , cleaning chemical, hotel cleaning, industrial cleaning, cost saving cleaning' />
@endsection
@section('content')
<div class="subpage-banner">
    <div class="subpage-banner-item" style="background:url(img/renewable-battery.jpg) 40% 40% / cover no-repeat;"></div>
</div>
<br /><br />
<!-- <div class="page-gallery">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="font-weight-bold text-secondary">Articles</h2>
            </div>
        </div>
        <hr />
        <div class="news-list" id="NewsList">
            @if(count($news) == 0)
            <div class="row">
                <div class="col">
                    <div class="d-flex align-items-center justify-content-center p-4 border">
                        <span>More news coming up soon!</span>
                    </div>
                </div>
            </div>
            @endif
            @foreach($news as $new)
            <div class="row pt-3">
                <div class="col-12 col-lg-6 pb-3">
                    <a class="blog-title" href='/news-gallery/{{$new['url']}}'><img src="/img/content/{{$new['id']}}/{{$new['image']}}" class="img-fluid" onerror="this.onerror=null;this.src='/img/image-not-found.png';" /></a>
                </div>
                <div class="col-12 col-lg-6">
                    <h2><a class="blog-title" href='/news-gallery/{{$new['url']}}'>{!!$new["title"]!!}</a></h2>
                    <p class="text-muted">Posted on 21 Feb 2020</p>
                    <div class="blog-desc"><p>{!!$new["desc"]!!}</p></div>
                    <div class="pt-3">
                        <hr class="d-lg-block d-none" />
                        <a class="btn btn-secondary text-white" href='/news-gallery/{{$new['url']}}'>Read More</a>
                    </div>
                </div>
            </div>
            <hr />
            @endforeach
        </div>
        <br />
        <div class="row">
            <div class="col">
                <h2 class="font-weight-bold text-secondary">Gallery</h2>
            </div>
        </div>
        <br/>
        <div class="gallery-list pb-5" id="GalleryList">
        @if(count($gallery) == 0)
            <div class="row">
                <div class="col">
                    <div class="d-flex align-items-center justify-content-center p-4 border">
                        <span>More content coming up soon!</span>
                    </div>
                </div>
            </div>
        @endif
        @if(count($gallery) > 0)
            <div class="blog-list">
                @foreach($gallery as $key => $group)
                @if(count($group['blog_images']) > 0)
                @foreach($group['blog_images'] as $index => $image)
                <a class="gallery-item" data-category="{!!$image['title']!!}" href="/img/content/{{$image['content_id']}}/{!!$image['image']!!}" class="d-block" data-size="1024x1024" data-caption="ProGreen-{!!$index+1!!}" data-id="camel" data-group="animal"/>
                    <img src="/img/content/{{$image['content_id']}}/{!!$image['image']!!}" class="img-fluid" 
                        data-image="/img/content/{{$image['content_id']}}/{!!$image['image']!!}" 
                        data-description="{!!$image['body']!!}" 
                        data-category="{!!$image['title']!!}"
                        alt="{!!$image['title']!!}" />
                </a>                
                @endforeach
                @endif
                @endforeach
            </div>
        @endif
        </div>
    </div>
</div> -->
<style>
    .text-property-title {}

    .text-property-header {
        text-align: center;
        font-family: PT Serif;
        color: #c5a254;
        font-size: 20px;
    }

    .text-property-title {
        text-align: center;
        font-family: PT Serif;
        color: #002569;
        font-size: 28px;
        font-weight: 500;
    }

    .text-property-smalltitle {
        text-align: center;
        font-family: Muli;
        color: #c5a254;
        font-size: 25px;
        font-weight: 700;

    }

    .text-property-content {
        text-align: center;
        font-family: Muli;
        color: #002569;
        font-size: 23px;
        font-weight: 700;

    }

    .text-property-content-end {
        text-align: center;
        font-family: Muli;
        color: #c5a254;
        font-size: 20px;
        font-weight: 700;

    }
</style>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="text-property-header">TRIVE</div>
            <div class="text-property-title">Our Core Team</div>
            <div class="text-property-smalltitle">
                The value of Trive Group Property Berhad is represented by<br /> its core team of dedicated and
                experienced management team.
            </div>
        </div>
    </div>
    <br /><br />

    <div class="row">
        <div class="col-12 col-sm-3 text-center">
            <img src="img/engineer3.jpg" style="height:200px" class="img-fluid" alt="{!!$config['company_name']!!}" />
            <br /><br />
            <img src="img/engineer1.jpg" style="height:200px" class="img-fluid" alt="{!!$config['company_name']!!}" />
        </div>
        <div class="col-12 col-sm-3 text-center">
            <img src="img/discussion1.jpg" style="height:200px" class="img-fluid" alt="{!!$config['company_name']!!}" />
            <br /><br />
            <img src="img/engineer2.jpg" style="height:200px" class="img-fluid" alt="{!!$config['company_name']!!}" />
        </div>
        <div class="col-6 col-sm-5" style="padding-left:80px">
            <br /><br /><br /><br /><br /><br /><br />

            <div class="text-property-content">
                <p>
                    Engineers and executives have more than 10 years’ experience in their aspects
                </p>
                <br />
            </div>
        </div>

    </div>
</div>
<br /><br /><br />

<div class="container">
    <div class="row">
        <div class="col-6 col-sm-6" style="padding-left:80px">
            <br /><br /><br /><br />
            <div class="text-property-content">
                <p>
                    Our core team member are specialized in their own aspect
                    included automobile, aerospace, telecommunications,
                    power supply, consumer electronics, batteries,
                    military equipment.
                </p>
                <br />
            </div>
        </div>
        <div class="col-12 col-sm-2 text-center">
            <img src="img/electric.jpg" style="height:180px" class="img-fluid" alt="{!!$config['company_name']!!}" />
            <br /><br />
            <img src="img/battery.jpg" style="height:180px" class="img-fluid" alt="{!!$config['company_name']!!}" />
        </div>
        <div class="col-12 col-sm-2 text-center">
            <img src="img/teleco.jpg" style="height:180px" class="img-fluid" alt="{!!$config['company_name']!!}" />
            <br /><br />
            <img src="img/aerospace.jpg" style="height:180px" class="img-fluid" alt="{!!$config['company_name']!!}" />
        </div>
        <div class="col-12 col-sm-2 text-center">
            <img src="img/automobile.jpg" style="height:180px" class="img-fluid" alt="{!!$config['company_name']!!}" />
            <br /><br />
            <img src="img/consumer_eletric .png" style="height:180px" class="img-fluid"
                alt="{!!$config['company_name']!!}" />
        </div>
    </div>
    <br /><br />
    <div class="text-property-content-end">
        <p>
            " As a pioneer in the green energy storage solution provider, and with Malaysian Government assistance
            <br />we can bring the know-how gained to a different level and to compete on world stage. "
        </p>
        <br />
    </div>
</div>
<br /><br />


<div class="engaging-customer"
    style="background: url(img/cta-bg_02.jpg);background-repeat: no-repeat; background-size: cover; background-position: top bottom;">
    @include('partial.engaging')
</div>
@endsection
@section('custom_style')
<style>

</style>
@endsection
@section('javascript')
<script>
    $(function () {
        // $("#GalleryList").unitegallery({
        //     gallery_theme: "tiles",
        //     tile_enable_textpanel: true,
        //     tile_textpanel_title_text_align: "center",
        //     tiles_type: "justified",
        // 	tiles_justified_row_height: 200,
        // })

        var $grid = null;
        $(window).on("load", function () {
            $grid = $('.gallery-list').isotope({
                itemSelector: '.gallery-item',
                columnWidth: '.gallery-item',
                percentPosition: true
            });

            // maually trigger resize to let gallery recalculate
            $(window).trigger('resize');
        });

        $(".btn-filter-gallery").click(function () {
            var category = $(this).data('category');
            $grid.isotope({
                filter: "[data-category='" + category + "']"
            });
        });

        $(".btn-filter-all").click(function () {
            $grid.isotope({
                filter: "*"
            });
        });

        $(".gallery-item").SmartPhoto({
            useOrientationApi: false,
            useHistoryApi: false,
        });
    })
</script>
<script>
    $(function () {})
</script>
@endsection