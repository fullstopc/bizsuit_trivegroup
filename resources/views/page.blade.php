@extends('layout')
@section('before_wrapper')
<div class="fullpage-slider-wrapper">
    <div class="slider-list">
        <div class="slider-item">
            <img src="img/banner-01.jpg" />
            <div class="slider-content">
                <div class='slider-tagline'>
                    Proposal Ring Collection
                </div>
                <div class='slider-subtagline'>
                    It just more than words.
                </div>
            </div>
        </div>
        <div class="slider-item">
            <img src="img/banner-02.jpg" />
            <div class="slider-content">
                
            </div>
        </div>
        <div class="slider-item">
            <img src="img/banner-03.jpg" />
            <div class="slider-content">

            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
{!!$page['body']!!}
@endsection
@section('javascript')
<script>
$(function(){
    var initStickyFooter = function(){
        var heights = {
            window: $(window).height(),
            header: $('#header').outerHeight(),
            footer: $('#footer').outerHeight(),
            content: $("#content").height(),
            html: 0
        };
        heights.html = heights.header + heights.footer + heights.content;
        if(heights.window > heights.html){
            $("#content").css({"height":(heights.window - heights.header - heights.footer) + 'px'})
        }
    }
    //initStickyFooter();
})
</script>
@endsection