window.$ = window.jQuery = require('jquery');

require('parsleyjs');
require('bootstrap');
require('slick-carousel');
require('isotope-layout/dist/isotope.pkgd');
require('smartphoto/js/jquery-smartphoto');